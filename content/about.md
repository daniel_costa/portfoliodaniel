
## Biografia
Nasci em São Paulo (SP) e cresci no bairro de Itaquera. Tenho 22 anos e atualmente moro em uma região próxima à Universidade de São Paulo (USP). Fiz o Ensino Fundamental no SESI e cursei o Ensino Médio no Instituto Federal de São Paulo (IFSP), junto com o Técnico em Eletrônica. Já estagiei em uma empresa de eletrônicos e sempre tive paixão pela área.