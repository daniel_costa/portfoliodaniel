+++
title = 'Carrinho'
date = 2024-10-11T00:34:04-03:00
draft = false
custom_class = "curriculo"
+++

Em 2023, junto aos meus colegas e professores da usp, realizei um projeto que desenvolvia uma máquina, denominada 'carrinho', que era controlada pelo celular, via bluetooth, e coletava objetos e depositava nos recipientes adequados.