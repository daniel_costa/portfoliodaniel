+++
title = 'Currículo'
date = 2024-10-11T00:34:04-03:00
draft = false
+++

### Daniel Lima Costa
### Brasileiro, Solteiro, 22 anos.
### E-mail: dlima.cost42@gmail.com
### Contato: (11) 123456789

### Sobre mim:
Estou buscando uma oportunidade de emprego, com o objetivo de aprimorar a minha carreira profissional, desenvolver minha habilidade e dar o meu melhor para contribuir com o crescimento da empresa. Sou proativo, competente e tenho facilidade de trabalhar em grupo. Disposição para trabalhar home office e/ou presencial na área de tecnologia, eletrônica , mecatrônica e mercado financeiro.

### Educação:
IFSP - Instituto Federal de Educação, Ciência e Tecnologia
         Técnico em Eletrônica Integrado ao Ensino Médio:    Fev (2017) - Dez (2020).

USP - Universidade de São Paulo - Campus São Paulo
         Bacharelado em Engenharia Mecatrônica:                 Mar (2022) - Presente.

### Experiencia:
EletronPlast Eletrônica
         Estágio em Eletrônica. Periodo: Junho (2019) - Jan (2020).

### Habilidades:

Linguagem de Programação: Python , C.
Ferramentas: Excel, AutoCAD.

### Idiomas:
Inglês: Intermediário. 
