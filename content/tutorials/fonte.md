+++
title = 'Fonte de Tensão'
date = 2024-10-11T00:34:14-03:00
draft = false
+++

Em 2018, no Instituto Federal de São Paulo, realizei um projeto de uma fonte de tensão que eu utilizo até hoje. Usando os componente adequados, a fonte varia de 0V até 23V em nível DC.
