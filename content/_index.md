
Sou Daniel Lima Costa, um estudante de Engenharia Mecatrônica que tem paixão por eletrônica. Estou no 6º semestre na Universidade de São Paulo (USP). Através deste portfólio mostrarei meu currículo e alguns projetos feitos por mim.

Aqui você verá:

- Uma pequena biografia minha
- Meus projetos
- Meu currículo
